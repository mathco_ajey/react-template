import React, { useContext, useEffect } from "react";
import './App.css';
import { useMsal, AuthenticatedTemplate, UnauthenticatedTemplate } from "@azure/msal-react";
import { ThemeProvider } from '@mui/material/styles';
import { theme } from './theme/theme';
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import Home from "./Components/Home/Home";
import Dashboard from "./Components/Dashboard/Dashboard";
import ErrorPage from "./Components/ErrorPage/ErrorPage";
import Login from "./Components/Login/Login";
import SnackBarComponent from "./Components/SnackBarComponent/SnackBarComponent";
import Header from "./Components/Header/Header";
import Records from "./Components/Records/Records";
import { GlobalContext } from "./context/Provider";

const App = () => {
  const { favState, favDispatch } = useContext(GlobalContext)


  useEffect(() => {
    favDispatch({ type: "SET_NAME", payload: "Demo User" })
    favDispatch({ type: "SET_USERNAME", payload: "demoUser@themathcompany.com" })
  }, [])

  return (
    <>
      <ThemeProvider theme={theme}>
        <AuthenticatedTemplate>
          <BrowserRouter>
            <Routes>
              <Route path='/' element={<Home />} />
              <Route path='/dashboard' element={<Dashboard />} />
              <Route path='/records' element={<Records />} />
              <Route path='/error' element={<ErrorPage />} />
              <Route path="*" element={<Navigate to="/error" />} />
            </Routes>
            <SnackBarComponent />
          </BrowserRouter>
        </AuthenticatedTemplate>

        <UnauthenticatedTemplate>
          <Login />
        </UnauthenticatedTemplate>
      </ThemeProvider>
    </>
  );
};

export default App;