import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import './index.css';
import { GlobalProvider } from "./context/Provider";
import { MsalProvider } from "@azure/msal-react";
import { PublicClientApplication } from "@azure/msal-browser";

// Configuration object constructed.
const config = {
    auth: {
      postLogoutRedirectUri: "/login", // Indicates the page to navigate after logout.
      navigateToLoginRequestUrl: false, // If "true", will navigate back to the original request location before processing the auth code response.
      clientId: "<client ID>", // DEV
      authority: "https://login.microsoftonline.com/<tenant ID>/",
      redirectUri: "http://localhost:3000", // app registertion
      
    },
    cache: {
      cacheLocation: "localStorage",
      storeAuthStateInCookie: false,
    },
    system: {
      loggerOptions: {
        loggerCallback: (level, message, containsPii) => {
          if (containsPii) {
            return;
          }
          return;
          // switch (level) {
          //     case LogLevel.Error:
          //         console.error(message);
          //         return;
          //     case LogLevel.Info:
          //         console.info(message);
          //         return;
          //     case LogLevel.Verbose:
          //         console.debug(message);
          //         return;
          //     case LogLevel.Warning:
          //         console.warn(message);
          //         return;
          // }
        }
      }
    }
  };

  // create PublicClientApplication instance
const publicClientApplication = new PublicClientApplication(config);

// Wrap your app component tree in the MsalProvider component
ReactDOM.render(
  <React.StrictMode>
    <MsalProvider instance={publicClientApplication}>
      <GlobalProvider>
        <App />
      </GlobalProvider>
    </ MsalProvider>
  </React.StrictMode>,
  document.getElementById("root")
);