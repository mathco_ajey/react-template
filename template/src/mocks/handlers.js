import { rest } from 'msw';

export const handlers = [
    rest.post(`${process.env.REACT_APP_BASE_URL}/accounts/login/`, (req, res, ctx) => {
        return res(
            ctx.status(200),
            ctx.json({ "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTY2MDcxNTY5NywiaWF0IjoxNjU5NDE5Njk3LCJqdGkiOiJhNDU4MGUyZTcyZDQ0YjdiYWJmYmU3MTAxY2YwOWZhMCIsInVzZXJfaWQiOjc2LCJlbWFpbCI6InJhZGhpa2EucHJpeWF2YXJkaGluaS1leHRAYWItaW5iZXYuY29tIn0.03IqfGX1sHWgfUqQd2-2g7LEWHxI2jZQh3mn8-OxwkA", "access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjU5NTA2MDk3LCJpYXQiOjE2NTk0MTk2OTcsImp0aSI6IjVkN2IyM2E3Yzc0ZDRhMzRhNGEzZTkyODhmYzA1ZWI3IiwidXNlcl9pZCI6NzYsImVtYWlsIjoicmFkaGlrYS5wcml5YXZhcmRoaW5pLWV4dEBhYi1pbmJldi5jb20ifQ.VF1ESbCY7r5JfKOD0uL5-TxbNTkj1cHSJz6SS7kzl6s" })
        )
    }),
    rest.get(`${process.env.REACT_APP_BASE_URL}/accounts/profile/john.doe@contoso.com`, (req, res, ctx) => {
        return res(
            ctx.status(200),
            ctx.json({
                data: [
                    {
                        email: "john.doe@contoso.com",
                        first_name: "John Doe",
                        is_active: true,
                        last_login: "2022-08-02T05:54:57.787246",
                        businessFuncId: 19,
                        countryId: 6,
                        roleId: 6,
                        countryName: "MAZ",
                        businessFuncName: "Portfolio",
                        roleName: "DataAnalyst"
                    }],
                code: 200,
                message: "success"
            })
        )
    })
    
]