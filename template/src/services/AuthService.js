export function signInClickHandler(instance) {
    // instance.loginPopup();
    instance.loginRedirect();
  }
  
export function signOutClickHandler(instance) {
    const logoutRequest = {
        account: instance.getAccountByHomeId("<clientID>"),
        postLogoutRedirectUri: "", 
    }
    instance.logoutRedirect(logoutRequest);
  }