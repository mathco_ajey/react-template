import axios from "axios";

const baseURL = process.env["REACT_APP_BASE_URL"]

let headers = {}

if(localStorage.accessToken){
    headers.Authorization = `Bearer ${localStorage.accessToken}`;
}

const axiosInstance = axios.create({
    baseURL : baseURL,
    headers,
})

export default axiosInstance;