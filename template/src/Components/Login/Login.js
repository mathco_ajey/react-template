import React from 'react'
import { signInClickHandler } from '../../services/AuthService';
import { useMsal } from "@azure/msal-react";
import { Button} from '@mui/material';
import logo from "../../assets/images/mathcoLogo.png"

function Login() {
    const { accounts, instance } = useMsal();

    const handleLoginClick = async () => {
        try {
            let login = await signInClickHandler(instance)
        } catch (error) {
            alert("error ", error)
        }
    }

    return (
        <div>
            <div className="login-card">
                <div className="logo">
                    <img src={logo} height="50px" alt="Brand Logo" />
                    <div className='brandName'>
                        <div className='brandName_child'>Brand</div>
                        <h3>Name</h3>
                    </div>

                </div>
                <div className="text">
                    <p>Description: </p>
                </div>
                <div className="button-container">
                    <Button onClick={handleLoginClick}>Login</Button>
                </div>
            </div>
        </div>
    )
}

export default Login