import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { MsalProvider } from '@azure/msal-react';
import userEvent from '@testing-library/user-event';
import { BrowserRouter, MemoryRouter } from 'react-router-dom';
import Header from '../Header';

import { MsalReactTester } from 'msal-react-tester';
import { GlobalProvider } from '../../../context/Provider'
import App from '../../../App';

let msalTester;
beforeEach(() => {
    // new instance of msal tester for each test
    msalTester = new MsalReactTester();

    // spy all required msal things
    msalTester.spyMsal();
});

afterEach(() => {
    msalTester.resetSpyMsal();
});

test('Header render correctly', async () => {
    msalTester.isLogged();
    render(
        <MsalProvider instance={msalTester.client}>
            <GlobalProvider>
                <BrowserRouter>
                    <Header />
                </BrowserRouter>
            </GlobalProvider>
        </MsalProvider>,
    );

    await msalTester.waitForRedirect();

    let dashboardText = await screen.getByText('Dashboard')
    expect(dashboardText).toBeInTheDocument()

    let recordsText = screen.getByText('Records')
    expect(recordsText).toBeInTheDocument()

});

test('test navigation', async () => {
    msalTester.isLogged();
    render(
        <MsalProvider instance={msalTester.client}>
            <GlobalProvider>
                <App />
            </GlobalProvider>
        </MsalProvider>,
    );

    await msalTester.waitForRedirect();

    let dashboardText = await screen.getByText('Dashboard')
    expect(dashboardText).toBeInTheDocument()

    let recordsText = screen.getByText('Records')
    expect(recordsText).toBeInTheDocument()

    userEvent.click(dashboardText)
    expect(screen.getByText('Dashboard')).toBeInTheDocument()

    let records = screen.getByText('Records')
    expect(records).toBeInTheDocument()
    userEvent.click(records)
    expect(screen.getByText('RecordsPage')).toBeInTheDocument()
    

});