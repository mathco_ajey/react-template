import { useMsal } from '@azure/msal-react';
import { Button } from '@mui/material';
import React, { useContext } from 'react'
import { NavLink } from 'react-router-dom'
import logo from '../../assets/images/mathcoLogo.png'
import { signOutClickHandler } from '../../services/AuthService'
import LogoutIcon from '@mui/icons-material/Logout';
import { GlobalContext } from '../../context/Provider';

function Header() {
    const { accounts, instance } = useMsal();
    const { favState, favDispatch } = useContext(GlobalContext)

    return (
        <div>
            <div style={{ padding: "0 18px", display: "flex", justifyContent: "space-between", alignItems: "center", boxShadow: "0px 3px 6px #00000029", borderRadius: "12px", }}>
                <div> <img src={logo} alt='Logo' height="30px" /> </div>
                <div style={{ display: 'flex', justifyContent: "center", alignItems: "center", gap: "20%", cursor: "pointer", }}>
                    <NavLink to="/" style={({ isActive }) => isActive ? { height: "50px", display: "flex", alignItems: "center", borderBottom: '4px solid #B83939', textDecoration: "none", padding: "0 30px", color: "black" } : { textDecoration: "none", cursor: "pointer", padding: "0 30px", color: "black" }}>Home</NavLink>
                    <NavLink to="/dashboard" style={({ isActive }) => isActive ? { height: "50px", display: "flex", alignItems: "center", borderBottom: '4px solid #B83939', textDecoration: "none", padding: "0 30px", color: "black" } : { textDecoration: "none", cursor: "pointer", padding: "0 30px", color: "black" }}>Dashboard</NavLink>
                    <NavLink to="/records" style={({ isActive }) => isActive ? { height: "50px", display: "flex", alignItems: "center", borderBottom: '4px solid #B83939', textDecoration: "none", padding: "0 30px", color: "black" } : { textDecoration: "none", cursor: "pointer", padding: "0 30px", color: "black" }}>Records</NavLink>
                </div>
                <div>
                    <span>{favState.name}</span>
                    <Button onClick={() => signOutClickHandler(instance)} > <LogoutIcon /> </Button>
                </div>

            </div>
        </div>
    )
}

export default Header