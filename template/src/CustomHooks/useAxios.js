import React, { useEffect, useState } from 'react';
import { InteractionRequiredAuthError, InteractionStatus } from "@azure/msal-browser";
import { AuthenticatedTemplate, useMsal } from "@azure/msal-react";
import axios from "axios";

function useAxios() {

    const { instance, inProgress, accounts } = useMsal();
    const [apiData, setApiData] = useState(null);


    useEffect(() => {
        if (!apiData && inProgress === InteractionStatus.None) {
            const accessTokenRequest = {
                scopes: ["user.read"],
                account: accounts[0]
            }
            instance.acquireTokenSilent(accessTokenRequest).then((accessTokenResponse) => {
                // Acquire token silent success
                let accessToken = accessTokenResponse.accessToken;
                // Call your API with token
                let config = {
                    headers: { "Authorization": `Bearer ${accessToken}` }
                }
                axios({
                    url: dataURL,
                    method: method,
                    headers: config.headers,
                    data: postData
                }).then((response) => { setApiData(response) });
            }).catch((error) => {
                if (error instanceof InteractionRequiredAuthError) {
                    instance.acquireTokenPopup(accessTokenRequest).then(function (accessTokenResponse) {
                        // Acquire token interactive success
                        let accessToken = accessTokenResponse.accessToken;
                        // Call your API with token
                        let config = {
                            headers: { "Authorization": `Bearer ${accessToken}` }
                        }
                        axios({
                            url: dataURL,
                            method: method,
                            headers: config.headers,
                            data: postData
                        }).then((response) => { setApiData(response) });
                    }).catch(function (error) {
                        // Acquire token interactive failure
                        console.log(error);
                    });
                }
                console.log(error);
            })
        }
    }, [instance, accounts, inProgress, apiData]);

}
export default useAxios;